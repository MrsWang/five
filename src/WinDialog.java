import javax.swing.*;
import java.awt.*;

public class WinDialog extends JDialog {


    public WinDialog(Frame owner, String title, FiveView fiveView) {
        super(owner, title);
        JButton button = new JButton("重新开始");
        int width = 60;
        int height = 35;
        button.setBounds((getX() - width) / 2, getY() - height - 5, width, height);
        button.addActionListener(e -> {
            fiveView.clear();
            fiveView.repaint();
            this.setVisible(false);
        });
        add(button);
    }
}
