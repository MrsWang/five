import javax.swing.*;
import java.awt.*;

public class StartMain {
    public static void main(String[] args) {
        JFrame jFrame = new JFrame();
        jFrame.setLayout((LayoutManager)null);
        jFrame.setBounds(100, 100, 600, 600);
        FiveView fiveView = new FiveView();
        fiveView.setBounds(0, 0, 511, 510);
        fiveView.addMouseListener(fiveView);
        jFrame.add(fiveView);
        jFrame.setVisible(true);
        fiveView.setjFrame(jFrame);
    }

}
