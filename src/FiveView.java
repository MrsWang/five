import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class FiveView extends JComponent implements MouseListener {

    private WinDialog winDialog;
    private static final int SPACE = 30;
    /**
     * 1：绘制白棋
     * 2：绘制黑棋
     */
    private int[][] drawType;
    private int count;
    private int row;
    private int col;
    private int lastRowCol = -1;
    // 父窗体
    private JFrame jFrame;

    FiveView() {
        System.out.println("\n游戏开始！\n");
    }

    public void doLayout() {
        super.doLayout();
        row = getWidth() / SPACE + 1;
        col = getHeight() / SPACE + 1;
        // TODO 这里应该保留原来的数组里的数据
        if (drawType == null) {
            drawType = new int[row][col];
        } else if (drawType.length != row || drawType[0].length != col) {
            drawType = new int[row][col];
        }

    }

    public void paint(Graphics g) {
        super.paint(g);
        int x = getX();
        int y = getY();
        int width = getWidth();
        int height = getHeight();
        Color color = new Color(4145151);
        g.setColor(color);
        g.fillRect(x, y, width, height);
        color = new Color(6710886);
        g.setColor(color);
        int drawX = x;
        int drawY;
        // 间隔 SPACE px画垂直线
        for(drawY = y; drawX < x + width; drawX += SPACE) {
            g.drawLine(drawX, y, drawX, y + height);
        }

        // 间隔 SPACE px绘制水平线
        while(drawY < y + height) {
            g.drawLine(x, drawY, x + width, drawY);
            drawY += SPACE;
        }

        for(int i = 0; i < this.drawType.length; ++i) {
            int[] row = this.drawType[i];

            for(int j = 0; j < row.length; ++j) {
                int cell = row[j];
                if (cell == 1) {
                    g.setColor(Color.WHITE);
                } else {
                    if (cell != 2) {
                        continue;
                    }

                    g.setColor(Color.BLACK);
                }

                int ox = j * SPACE + 5;
                int oy = i * SPACE + 5;
                g.fillOval(ox, oy, SPACE / 2, SPACE / 2);
            }
        }

    }


    /**
     * 计算是否五子连珠
     * @param r 行
     * @param c 列
     */
    private void calculate(int r, int c) {
        //  4中情况 - | / \

        boolean win = false;
        // 当前绘制旗子的类型
        int currentType = count % 2 + 1;
        // 横 start
        int left = c;
        while (left > 0) {
            if (drawType[r][left -1] == currentType) {
                left--;
            } else {
                break;
            }
        }

        int right = c;
        while (right < col - 1) {
            if (drawType[r][right+ 1] == currentType) {
                right++;
            } else {
                break;
            }
        }

        // 5子连珠 赢了
        if (right - left + 1 >= 5) {
            win = true;

        }
        // 横 end

        // 竖 start
        int top = r;
        int bottom = r;
        if (!win) {
            while (top > 0) {
                if (drawType[top - 1][c] == currentType) {
                    top--;
                } else {
                    break;
                }
            }
            while (bottom < row - 1) {
                if (drawType[bottom + 1][c] == currentType) {
                    bottom++;
                } else {
                    break;
                }
            }
            if (bottom - top + 1 >= 5) {
                win = true;
            }
        }
        // 竖 end

        // 左斜 / start
        if (!win) {
            top = bottom = r;
            left = right = c;
            while (top > 0 && right < col - 1) {
                if (drawType[top - 1][right + 1] == currentType) {
                    right++;
                    top--;
                } else {
                    break;
                }
            }
            while (bottom < row - 1 && left > 0) {
                if (drawType[bottom + 1][left - 1] == currentType) {
                    bottom++;
                    left--;
                } else {
                    break;
                }
            }
            if (bottom - top + 1 >= 5) {
                win = true;
            }
        }
        // 左斜 / end
        // 右斜 \ start
        if (!win) {
            top = bottom = r;
            left = right = c;
            while (top > 0 && left > 0) {
                if (drawType[top - 1][left - 1] == currentType) {
                    left--;
                    top--;
                } else {
                    break;
                }
            }
            while (bottom < row - 1 && right < col - 1) {
                if (drawType[bottom + 1][right + 1] == currentType) {
                    bottom++;
                    right++;
                } else {
                    break;
                }
            }
            if (bottom - top + 1 >= 5) {
                win = true;
            }
        }
        // 右斜 \ end
        if (win) {
            System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
            System.out.println((currentType == 1 ? "白旗" : "黑旗") + "胜利！");
            int x = jFrame.getX();
            int y = jFrame.getY();
            int width = jFrame.getWidth();
            int height = jFrame.getHeight();
            if (winDialog == null) {
                winDialog = new WinDialog(jFrame, "恭喜你，胜利了！", this);
            }
            winDialog.setBounds(x + width / 4, y + height / 4, width / 2, 100);
            winDialog.setVisible(true);
        }
    }

    public void mouseClicked(MouseEvent e) {

    }

    public void mousePressed(MouseEvent e) {

    }

    public void mouseReleased(MouseEvent e) {
        ++this.count;
        int type = this.count % 2 + 1;
        int clickX = e.getX();
        int clickY = e.getY();
        int r = clickY / SPACE;
        int l = clickX / SPACE;
        boolean isCalculate = true;
        if (this.drawType[r][l] != 0) {
            if ((r << 16) + l != lastRowCol) {
                return;
            }
            this.drawType[r][l] = 0;
            this.count -= 2;
            isCalculate = false;
        } else {

            this.drawType[r][l] = type;
            lastRowCol = (r << 16) + l;
        }
        System.out.println((type == 1 ? "白旗" : "黑旗") + "落在(" + r + "," + l +")点上。");
        if (isCalculate) {
            calculate(r, l);
        }
        this.repaint();
    }

    public void mouseEntered(MouseEvent e) {

    }

    public void mouseExited(MouseEvent e) {
    }


    public JFrame getjFrame() {
        return jFrame;
    }

    public void setjFrame(JFrame jFrame) {
        this.jFrame = jFrame;
    }


    public void clear() {
        drawType = new int[row][col];
        System.out.println("\n\n**************重新开始***********************\n\n");
    }

}
